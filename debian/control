Source: reglookup
Section: utils
Priority: optional
Maintainer: Debian Forensics <forensics-devel@lists.alioth.debian.org>
Uploaders: Christophe Monniez <christophe.monniez@fccu.be>
Build-Depends: debhelper (>= 9), dh-python, python-all,
 scons (>= 2.3), libtalloc2, libtalloc-dev, doxygen, docbook2x
Standards-Version: 3.9.6
Homepage: http://projects.sentinelchicken.org/reglookup/
Vcs-Browser: http://git.debian.net/?p=debian-forensics/reglookup.git
Vcs-Git: git://git.debian.net/git/debian-forensics/reglookup.git
X-Python-Version: >= 2.7

Package: reglookup
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: python-pyregfi
Description: utility to read and query Windows NT/2000/XP registry
 RegLookup is an small command line utility for reading and querying Microsoft
 Windows NT/2000/XP registries.
 .
 Currently the program allows one to read an entire registry and output it in a
 (mostly) standardized, quoted format. It also provides features for filtering
 of results based on registry path and data type.

Package: libregfi1
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Suggests: libregfi-dev
Description: shared C library for reglookup
 This package contains the associated C library for reglookup utility.

Package: libregfi-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends}, ${misc:Depends}, libregfi1 (= ${binary:Version}),
 libtalloc-dev
Recommends: reglookup-doc
Description: shared C library for reglookup (development files)
 This package contains the associated library for reglookup utility.
 .
 This package contains development files for libregfi.

Package: python-pyregfi
Architecture: all
Section: python
Depends: ${python:Depends}, ${misc:Depends}, libregfi1 (>= ${binary:Version})
Recommends: reglookup-doc
Description: Python wrappers for librgefi
 This package contains Python wrappers for reglookup. There are the
 low-level data structures for winsec library and C API mappings.

Package: reglookup-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, iceweasel | x-www-browser
Suggests: libregfi-dev, python-pyregfi
Description: developer documentation for libregfi and python-pyregfi
 This package contains the developer documentation for libraries libregfi and
 python-pyregfi.
